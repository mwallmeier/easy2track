from db.abstract import Migrate
from sqlalchemy import types

def migrate_db():
    migration = Migrate()

    # 25.05.2020
    migration.add_column(table_name="contacts", column_name="deletedtime", data_type=types.DateTime())
    #migration.execute("Set deletedtime to last possible date at the same day if null", 'UPDATE contacts  SET deletedtime = DATE_ADD(DATE_ADD(DATE(addedtime), INTERVAL 1 DAY), INTERVAL -1 SECOND) WHERE deletedtime IS NULL')
    migration.add_column(table_name="contacts", column_name="form_data_hash", data_type=types.Text())
