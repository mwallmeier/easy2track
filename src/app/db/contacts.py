from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Contact(DbEntity, Base):
    __tablename__ = 'contacts'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'mysql_collate': 'utf8_general_ci'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    consumer = Column(String(255))
    login = Column(String(255))
    external_id = Column(String(255))
    correlation_id = Column(String(255))
    payload = Column(Text)
    addedtime = Column(DateTime)
    deletedtime = Column(DateTime)
    form_data_hash = Column(Text)

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Contacts(id='%s', consumer='%s', login='%s', addedtime='%s', deletedtime='%s')>\r\n%s" % (self.id, self.consumer, self.login, self.addedtime, self.deletedtime, self.payload)

    def save(self):
        try:
            self.session.add(self)
            self.session.commit()
            self.session.flush()
        except Exception as e:
            print(e)
            self.rollback()

    def commit(self):
        self.session.commit()

    def add(self):
        self.session.add(self)
        self.session.commit()

    def rollback(self):
        self.session.rollback()


