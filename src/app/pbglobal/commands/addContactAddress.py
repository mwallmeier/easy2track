from .abstract import Command
import datetime
import hashlib
import json

class addContactAddress(Command):
    def __init__(self, owner=None, login=None, addedtime=None, correlation_id=None, skip_validation=False, payload=None):
        super().__init__()
        self.owner = owner
        self.payload = payload
        self.addedtime = addedtime
        self.deletedtime = None
        self.address_hash = "" if self.payload is None else hashlib.md5(self.payload.encode('utf-8')).hexdigest()

        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.addedtime is not None, "datetime may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        if type(self.payload) is dict:
            self.payload = json.dumps(self.payload)
            self.address_hash = "" if self.payload is None else hashlib.md5(self.payload.encode('utf-8')).hexdigest()
            self.payload = self.payload
        elif not len(self.address_hash) > 0:
            self.address_hash = "" if self.payload is None else hashlib.md5(self.payload.encode('utf-8')).hexdigest()
        return True
