from .abstract import Command
from config import Config

class resetDemoAccount(Command):
    def __init__(self, skip_validation=False):
        super().__init__()
        print("DELETING ALL EVENT_RELATED COMMANDS FROM DEMO-ACCOUNT")
        self.session = Config.db_session
        sql_command = 'DELETE FROM events WHERE owner="37d992e0-03bd-45c6-9492-789a2f4ae0fa" AND type IN ("createNewAppointment", "changeAppointment","deleteAppointment")'
        self.session.execute(sql_command)
        self.session.commit()
        self.session.close()

        if not skip_validation:
            self.validate()

    def validate(self):
        return dict(result=True)
