from .abstract import Command


class deleteLogin(Command):
    def __init__(self, username=None, skip_validation=False):
        super().__init__()
        self.username = username

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.username is not None, "username may not be empty"
        return dict(result=True)
