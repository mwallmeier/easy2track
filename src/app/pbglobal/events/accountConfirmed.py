from .abstract import Event


class accountConfirmed(Event):
    def __init__(self, registration_key=None, correlation_id=None, skip_validation=False):
        super().__init__()
        self.registration_key = registration_key
        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.registration_key is not None, "username may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
