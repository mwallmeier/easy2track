import os
import os.path
import datetime
from M2Crypto import BIO, Rand, SMIME
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import boto3
import smtplib, ssl

from email.message import Message
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

signMail = True
if os.path.isfile(os.getenv("MAIL_CERTIFICATES_KEY_PATH")):
    ssl_key = os.getenv("MAIL_CERTIFICATES_KEY_PATH")
else:
    signMail = False
if os.path.isfile(os.getenv("MAIL_CERTIFICATES_CRT_PATH")):
    ssl_cert = os.getenv("MAIL_CERTIFICATES_CRT_PATH")
else:
    signMail = False

sender = os.getenv("SMTP_USERNAME") if os.getenv("SMTP_USERNAME") else "notset@example.com"

def send_mail(recipient, subject, body):
    if isinstance(recipient, str):
        to = [recipient]
    login = os.getenv("SMTP_USERNAME")
    password = os.getenv("SMTP_PASSWORD")

    msg = MIMEMultipart()

    basemsg = MIMEText(body)
    msg.attach(basemsg)

    msg_str = msg.as_string()
    if signMail:
        buf = BIO.MemoryBuffer(msg_str.encode())

        # load seed file for PRNG
        Rand.load_file('/tmp/randpool.dat', -1)
        smime = SMIME.SMIME()

        # load certificate
        smime.load_key(ssl_key, ssl_cert)

        # sign whole message
        p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

        # create buffer for final mail and write header
        out = BIO.MemoryBuffer()
        out.write('From: %s\n' % sender)
        out.write('To: %s\n' % COMMASPACE.join(to))
        out.write('Date: %s\n' % formatdate(localtime=True))
        out.write('Subject: %s\n' % subject)
        out.write('Auto-Submitted: %s\n' % 'auto-generated')

        # convert message back into string
        buf = BIO.MemoryBuffer(msg_str.encode())

        # append signed message and original message to mail header
        smime.write(out, p7, buf)

        # load save seed file for PRNG
        Rand.save_file('/tmp/randpool.dat')

        content = out.read()
    else:
        content = msg_str

    # Log in to server using secure context and send email
    if os.getenv("SMTP_SSL") == "true":
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
            server.ehlo()
            if os.getenv("SMTP_AUTH_NEEDED") == "true":
                server.login(login, password)
            server.sendmail(sender, to, content)
            server.close()
    else:
        with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
            server.ehlo()
            if os.getenv("SMTP_AUTH_NEEDED") == "true":
                server.login(login, password)
            server.sendmail(sender, to, content)
            server.close()


    print('Email(s) Sent')

def send_mail_with_attachment(recipient, subject, body, attachment_file_names = []):
    if isinstance(recipient, str):
        to = [recipient]
    login = os.getenv("SMTP_USERNAME")
    password = os.getenv("SMTP_PASSWORD")

    msg = MIMEMultipart()

    basemsg = MIMEText(body)
    msg.attach(basemsg)
    for attachment_name in attachment_file_names:
        with open(attachment_name, "rb") as attachment_file:
            content = attachment_file.read()
            filename = attachment_name.split("/")[-1]

            part = MIMEApplication(content)
            part.add_header('Content-Disposition', 'attachment', filename=filename)
            msg.attach(part)


    msg_str = msg.as_string()
    if signMail:
        buf = BIO.MemoryBuffer(msg_str.encode())

        # load seed file for PRNG
        Rand.load_file('/tmp/randpool.dat', -1)
        smime = SMIME.SMIME()

        # load certificate
        smime.load_key(ssl_key, ssl_cert)

        # sign whole message
        p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

        # create buffer for final mail and write header
        out = BIO.MemoryBuffer()
        out.write('From: %s\n' % sender)
        out.write('To: %s\n' % COMMASPACE.join(to))
        out.write('Date: %s\n' % formatdate(localtime=True))
        out.write('Subject: %s\n' % subject)
        out.write('Auto-Submitted: %s\n' % 'auto-generated')

        # convert message back into string
        buf = BIO.MemoryBuffer(msg_str.encode())

        # append signed message and original message to mail header
        smime.write(out, p7, buf)

        # load save seed file for PRNG
        Rand.save_file('/tmp/randpool.dat')

        content = out.read()
    else:
        content = msg_str

    # Log in to server using secure context and send email
    if os.getenv("SMTP_SSL") == "true":
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
            server.ehlo()
            if os.getenv("SMTP_AUTH_NEEDED") == "true":
                server.login(login, password)
            server.sendmail(sender, to, content)
            server.close()
    else:
        with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
            server.ehlo()
            if os.getenv("SMTP_AUTH_NEEDED") == "true":
                server.login(login, password)
            server.sendmail(sender, to, content)
            server.close()

    print('Email(s) Sent')


