from flask import Flask, jsonify, request, redirect, after_this_request, render_template, url_for, render_template
import json
import os
from planblick.logger import Logger
from config import Config
import subprocess
from pbglobal.pblib.pbrsa import encrypt, decrypt
from mail_send import send_mail, send_mail_with_attachment
import random
import string
from flask import send_file
import pyminizip
import qrcode
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import urllib
import csv
from openpyxl import Workbook
import datetime
import hashlib
from db.contact_addresses import ContactAddress

app = Flask(__name__)
#TODO: use sio instead of this custom log-server implementation
#logserver_host = json.loads(os.getenv("CLUSTER_CONFIG")).get("logserver")
#log = Logger(logserver_host, 9999, consoleLog=True if len(logserver_host) > 0 else True).getLogger()


@app.route('/sc', methods=["GET"])
def shortcode_action():
    code = request.args.get("c")
    full_urls = {}

    redirect_url = full_urls.get(code, None)
    if redirect_url is not None:
        return redirect(redirect_url, code=302)
    else:
        return redirect("https://www.easy2track.de", code=302)

@app.route('/easy2track/contact_address', methods=["GET"])
def contact_address_action():
    consumer = request.headers.get("X-Consumer-Id", "UNKNOWN")
    
    sql = f'SELECT * FROM contact_addresses WHERE owner=:consumer and deletedtime is null order by addedtime desc limit 1'
    result = Config.db_session.execute(sql, {"consumer": urllib.parse.unquote(consumer)}).first()
    Config.db_session.commit()
    address_hash = result["address_hash"]
    result = json.loads(result["payload"])
    result["hash"] = address_hash

    if result is not None:
        return jsonify({"result": result}), 200
    else:
        return jsonify({"result": None}), 404



@app.route('/easy2track/qr', methods=["POST"])
def qr_action():
    request_data = request.get_json(silent=False)
    request_data["check"] = {}
    for key, value in request_data["data"].items():
        if key == "street":
            request_data["check"][key] = value.split(" ")[-1]
        if key == "lastname" or key == "firstname" or key == "zipcode":
            request_data["check"][key] = str(value[:1]) + "..." + str(value[-1])
        if key == "phone":
            request_data["check"][key] = str(value[-4:])
        if key == "email":
            request_data["check"][key] = str(value[:3]) + "..." + value.split("@")[-1]

    request_data["data"] = encrypt(json.dumps(request_data["data"]))

    logo = Image.open('/src/app/ressources/logo.png')

    # ERROR_CORRECT_L ~ 7%
    # ERROR_CORRECT_M ~ 15%
    # ERROR_CORRECT_Q ~ 25%
    # ERROR_CORRECT_H ~ 30%

    qr_big = qrcode.QRCode(
        version=None,
        box_size=4,
        border=8,
        error_correction=qrcode.constants.ERROR_CORRECT_L
    )
    qr_big.add_data(json.dumps(request_data))
    qr_big.make(fit=True)
    img_qr_big = qr_big.make_image(fill_color="#000000", back_color="#ffffff").convert('RGB')
    size = img_qr_big.size[0] / 5, img_qr_big.size[0] / 5
    logo.thumbnail(size, Image.ANTIALIAS)
    pos = ((img_qr_big.size[0] - logo.size[0]) // 2, (img_qr_big.size[1] - logo.size[1]) // 2)

    img_qr_big.paste(logo, pos)
    tmp_filename = "/tmp/" + request_data["check"]["firstname"] + ".png"
    img_qr_big.save(tmp_filename)

    img = Image.open(tmp_filename)
    draw = ImageDraw.Draw(img)

    font = ImageFont.truetype("/src/app/ressources/Roboto-Regular.ttf", 25)

    draw.text((30, 0), "Name: " + request_data["check"]["firstname"] + " Tel: " + request_data["check"]["phone"],
              (0, 0, 0), font=font)
    img.save(tmp_filename)

    @after_this_request
    def cleanup(response):
        os.remove(tmp_filename)
        return response

    return send_file(
        tmp_filename,
        mimetype='image/png',
    )

@app.route('/easy2track/qr/v2', methods=["POST"])
def v2qr_action():
    consumer = request.headers.get("X-Consumer-Username", "UNKNOWN")
    request_data = request.get_json(silent=False)

    logo = Image.open('/src/app/ressources/logo.png')

    contact_address = None
    query = Config.db_session.query(ContactAddress).filter(
    ContactAddress.address_hash == request_data.get("address_hash"))
    contact_address = query.first()
    Config.db_session.close()
    print("CONTACT ADDRESS", contact_address)
    address_data = json.loads(contact_address.payload)
    check_data = {}
    check_data["firstname"] = str(address_data.get("firstname", " ")[:1]) + "..." + str(address_data.get("firstname", " ")[-1])
    check_data["phone"] = str(address_data.get("phone", "n.V.")[-4:])

    # ERROR_CORRECT_L ~ 7%
    # ERROR_CORRECT_M ~ 15%
    # ERROR_CORRECT_Q ~ 25%
    # ERROR_CORRECT_H ~ 30%

    qr_big = qrcode.QRCode(
        version=None,
        box_size=6,
        border=8,
        error_correction=qrcode.constants.ERROR_CORRECT_L
    )

    payload = {
        "hash": contact_address.address_hash,
        "id": hashlib.md5(str(contact_address.id).encode()).hexdigest()
    }
    qr_big.add_data(json.dumps(payload))
    qr_big.make(fit=True)
    img_qr_big = qr_big.make_image(fill_color="#000000", back_color="#ffffff").convert('RGB')
    size = img_qr_big.size[0] / 5, img_qr_big.size[0] / 5
    logo.thumbnail(size, Image.ANTIALIAS)
    pos = ((img_qr_big.size[0] - logo.size[0]) // 2, (img_qr_big.size[1] - logo.size[1]) // 2)

    #img_qr_big.paste(logo, pos)
    tmp_filename = "/tmp/" + check_data["firstname"] + ".png"
    img_qr_big.save(tmp_filename)

    img = Image.open(tmp_filename)
    draw = ImageDraw.Draw(img)

    font = ImageFont.truetype("/src/app/ressources/Roboto-Regular.ttf", 25)

    draw.text((40, 0), "Name: " + check_data["firstname"] + " Tel: " + check_data["phone"],
              (0, 0, 0), font=font)
    img.save(tmp_filename)

    @after_this_request
    def cleanup(response):
        os.remove(tmp_filename)
        return response

    return send_file(
        tmp_filename,
        mimetype='image/png',
    )


@app.route('/is_checkedin', methods=["POST"])
def is_checkedin_action():
    request_data = request.get_json(silent=False)
    consumer = request.headers.get("X-Consumer-Username", "UNKNOWN")
    consumer_id = request.headers.get("X-Consumer-ID", "UNKNOWN")
    hash = request_data.get("hash") if request_data.get("hash") else ""
    sql = f'SELECT * FROM contacts WHERE (consumer=:consumer or consumer=:consumer_id) AND form_data_hash = :form_data_hash and addedtime is not null and deletedtime is null'
    results = Config.db_session.execute(sql,
                                        {"consumer": urllib.parse.unquote(consumer), "consumer_id": urllib.parse.unquote(consumer_id), "form_data_hash": hash}).first()
    print("RESULTS", results)
    Config.db_session.commit()

    if results is not None:
        return jsonify({"result": True}), 200
    else:
        return jsonify({"result": False}), 200


@app.route('/request_export_contacts', methods=["POST"])
def request_export_contacts_action():
    request_data = request.get_json(silent=False)
    consumer = request.headers.get("X-Consumer-Username", "UNKNOWN")
    consumer_id = request.headers.get("X-Consumer-ID", "UNKNOWN")
    zip_pass = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
    from_date = request_data.get("from_date") if request_data.get("from_date") else "2010-01-01 00:00:00"
    to_date = request_data.get("to_date") if request_data.get("to_date") else "2300-01-01 00:00:00"
    requester_name = request_data.get("name") if request_data.get("name") else False
    requester_email = request_data.get("email") if request_data.get("email") else False
    requester_phone = request_data.get("phone") if request_data.get("phone") else False
    requester_message = request_data.get("message") if request_data.get("message") else False

    email_body = f"{requester_name} hat für den Consumer '{consumer}' den im Anhang befindlichen Export für die Zeit von {from_date} bis {to_date} angefragt.\n" \
                 f"Wenn die Anfrage in Ordnung zu sein scheint, diese Email mit angepasstem Text weiterleiten an {requester_email}.\n" \
                 f"Für Rückfragen wurde folgende Telefonnumer angegeben: {requester_phone}\n" \
                 f"Das generierte Passwort für die Zip-Datei lautet: {zip_pass}\n" \
                 f"Nachricht des Anfragenden:\n" \
                 f"{requester_message}"

    print("FETCHING CONTACTS FOR CONSUMER", email_body)

    contacts = []
    sql = f'SELECT * FROM contacts WHERE (consumer=:consumer or consumer=:consumer_id) AND addedtime >= :from_date and addedtime <= :to_date'
    results = Config.db_session.execute(sql,
                                        {"consumer": consumer, "consumer_id": urllib.parse.unquote(consumer_id), "from_date": from_date, "to_date": to_date}).fetchall()

    for contact in results:
        contact_data = json.loads(contact.payload)
        contact_data["form_data"] = json.loads(decrypt(contact_data["form_data"])) if contact_data.get(
            "form_data") is not None else ""
        contact_data["email"] = decrypt(contact_data.get("email", "")) if contact_data.get("email") is not None and len(
            contact_data.get("email", "")) > 1 else contact_data["form_data"].get("email", "")

        contacts.append(contact_data)

    Config.db_session.commit()
    Config.db_session.close()

    with open("/tmp/data.json", "w") as json_file:
        json_file.write(json.dumps(contacts))

    csv_file = open("/tmp/data.csv", "w", newline='')
    f = csv.writer(csv_file)
    wb = Workbook()
    # grab the active worksheet
    ws = wb.active

    # Define headers
    headers = ["Nachname", "Vorname", "Strasse", "PLZ", "Stadt", "Telefon", "Email", "Anwesend von", "Anwesend bis"]
    # Write CSV Header
    f.writerow(headers)
    # Write excel header
    ws.append(headers)

    for contact in contacts:
        data = contact.get("form_data")
        datalist = [data.get("lastname"),
                    data.get("firstname"),
                    data.get("street"),
                    data.get("zipcode"),
                    data.get("city"),
                    data.get("phone"),
                    data.get("email"),
                    contact.get("addedtime", ""),
                    contact.get("deletedtime", "")]
        f.writerow(datalist)
        ws.append(datalist)

    csv_file.close()
    wb.save("/tmp/data.xlsx")

    pyminizip.compress_multiple(["/tmp/data.json", "/tmp/data.csv", "/tmp/data.xlsx"], [], "/tmp/data.zip", zip_pass, 9)
    # pyminizip.compress("/tmp/data.json", None, "/tmp/data.zip", zip_pass, 9)
    send_mail_with_attachment(recipient=os.getenv("EASY2TRACK_EXPORT_REQUEST_MAIL_RECIPIENT"), subject="Easy2Track Export Request", body=email_body,
                              attachment_file_names=["/tmp/data.zip"])

    send_mail(requester_email, "Easy2Track Export-Anfrage",
              f"Hallo {requester_name},\n\n wir haben Deine Anfrage für den Export für die Zeit von {from_date} bis {to_date} erhalten und werden die gewünschten Daten schnellstmöglich an {requester_email} schicken.\n\n Dein Easy2Track-Team")
    os.remove("/tmp/data.json")
    os.remove("/tmp/data.csv")
    os.remove("/tmp/data.xlsx")
    os.remove("/tmp/data.zip")

    return jsonify({"result": True}), 200


@app.route('/current_visitors', methods=["GET"])
def current_visitors_action():
    consumer = request.headers.get("X-Consumer-Username", "UNKNOWN")
    today = datetime.datetime.today().strftime('%Y-%m-%d 00:00:00')

    # sql = f'SELECT * FROM contacts WHERE owner=:account_id AND  (endtime > :start_date AND starttime BETWEEN :start_date AND :end_date OR starttime < :end_date AND endtime BETWEEN :start_date AND "{end_date}"  OR (starttime < :start_date AND endtime > :end_date))'
    # results = Config.db_session.execute(sql, {"start_date": start_date, "end_date": end_date, "account_id": account_id}).fetchall()
    sql = f'SELECT * FROM contacts WHERE consumer=:consumer AND addedtime >= :today'
    results = Config.db_session.execute(sql, {"consumer": consumer, "today": today}).fetchall()
    Config.db_session.commit()
    Config.db_session.close()

    current_visitors = []
    for contact in results:
        checkin_time = contact["addedtime"]
        #checkin_time = datetime.strptime(checkin_time, '%Y-%m-%d %H:%M:%S')
        checkout_time = contact["deletedtime"]
        #checkout_time = datetime.strptime(checkout_time, '%Y-%m-%d %H:%M:%S')
        if checkout_time is not None:
            duration = (checkout_time - checkin_time).seconds / 60
        else:
            duration = None
        current_visitors.append({"id": contact["form_data_hash"],
                                 "checkedin": checkin_time.strftime('%Y-%m-%d %H:%M:%S'),
                                 "checkedout": checkout_time.strftime('%Y-%m-%d %H:%M:%S') if checkout_time is not None else checkout_time,
                                 "duration": duration,
                                 "login": contact["login"]})

    return jsonify(current_visitors), 200




@app.route('/contacts', methods=["GET"])
def events_action():
    consumer = request.headers.get("X-Consumer-Username", "UNKNOWN")
    consumer_id = request.headers.get("X-Consumer-ID", "UNKNOWN")
    decrypt_email = True if request.args.get("decrypt") is not None else False
    send_emails = True if request.args.get("send") is not None else False
    output_json = True if request.args.get("format") == "json" else False
    output_zip = True if request.args.get("format") == "zip" or request.args.get("format") is None else False
    zip_pass = request.args.get("password") if request.args.get("password") is not None else "noneshallpassPB@2020"
    from_date = request.args.get("from_date") if request.args.get("from_date") else "2010-01-01 00:00:00"
    to_date = request.args.get("to_date") if request.args.get("to_date") else "2099-01-01 00:00:00"

    print("FETCHING CONTACTS FOR CONSUMER", consumer)

    contacts = []
    # start_date = datetime.fromisoformat(request.args.get("start"))
    # end_date = datetime.fromisoformat(request.args.get("end"))

    # sql = f'SELECT * FROM contacts WHERE owner=:account_id AND  (endtime > :start_date AND starttime BETWEEN :start_date AND :end_date OR starttime < :end_date AND endtime BETWEEN :start_date AND "{end_date}"  OR (starttime < :start_date AND endtime > :end_date))'
    # results = Config.db_session.execute(sql, {"start_date": start_date, "end_date": end_date, "account_id": account_id}).fetchall()
    sql = f'SELECT * FROM contacts WHERE (consumer=:consumer or consumer=:consumer_id) AND addedtime >= :from_date and addedtime <= :to_date'
    results = Config.db_session.execute(sql,
                                        {"consumer": consumer, "consumer_id": urllib.parse.unquote(consumer_id), "from_date": from_date, "to_date": to_date}).fetchall()

    for contact in results:
        contact_data = json.loads(contact.payload)

        if decrypt_email or output_zip:
            contact_data["form_data"] = json.loads(decrypt(contact_data["form_data"])) if contact_data.get(
                "form_data") is not None else ""
            contact_data["email"] = decrypt(contact_data["email"]) if contact_data["email"] is not None and len(contact_data["email"]) > 1 else contact_data[
                "form_data"].get("email", "")
        if send_emails:
            if (not decrypt_email):
                contact_data["form_data"] = json.loads(decrypt(contact_data["form_data"])) if contact_data.get(
                    "form_data") is not None else ""
                contact_data["email"] = decrypt(contact_data["email"]) if contact_data["email"] is not None and  len(contact_data["email"]) > 1 else \
                contact_data["form_data"].get("email", "")

            send_mail(contact_data["email"], "Crowdsoft Notfall Nachricht",
                      f"Hallo {contact_data.get('form_data', {}).get('contact_firstname', '')} {contact_data.get('form_data', {}).get('contact_lastname', '')}. Leider müssen wir Dir mitteilen dass Du bei Deinem letzten Besuch bei uns in Kontakt mit einem Corona-Patienten gekommen sein könntest. Dein Bart-Ab-Team!")

        contacts.append(contact_data)

    Config.db_session.commit()
    Config.db_session.close()

    if output_json:
        return jsonify(contacts), 200

    if output_zip:
        from flask import send_file
        import pyminizip

        with open("/tmp/data.json", "w") as json_file:
            json_file.write(json.dumps(contacts))

        pyminizip.compress("/tmp/data.json", None, "/tmp/data.zip", zip_pass, 9)

        return send_file(
            "/tmp/data.zip",
            mimetype='application/zip',
            as_attachment=True,
            attachment_filename='data.zip'
        )


@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.route('/metrics', methods=["GET"])
def metrics_action():
    command = 'top -b -n 1'  # or whatever you use
    output = subprocess.Popen(command, stdout=subprocess.PIPE).communicate[0]

    # parse output here and extract cpu usage. this is super-dependent
    # on the layout of your system monitor output
    cpuline = output.split('\n')[2]

    # +like you did here:
    print(output)
    return jsonify("NYI"), 200


@app.route('/network', methods=["GET"])
def network_action():
    network_config = Config()
    network_config = network_config.bindings

    return jsonify(network_config), 200


@app.route('/favicon.ico', methods=["GET"])
def favicon_action():
    return redirect("/static/favicon.ico", code=302)


@app.before_request
def before_request():
    payload = request.json
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)


@app.teardown_appcontext
def shutdown_session(exception=None):
    Config.db_session.remove()


@app.after_request
def after_request(response):
    """ Logging after every request. """
    if request.full_path not in ["/metrics?", "/metrics", "/healthz", "/healthz?"]:
        data = {}
        data["account_id"] = request.headers.get("X-Consumer-Id", "UNKNOWN")
        data["username"] = request.headers.get("X-Credential-Username", "UNKNOWN")
        data["consumername"] = request.headers.get("X-Consumer-Username", "UNKNOWN")
        # log(" |-| ".join([json.dumps(data), request.remote_addr, request.method, request.scheme, request.full_path,
        #                   response.status]))
    return response
