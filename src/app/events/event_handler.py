import json

from pika.exceptions import ChannelClosedByBroker
from sqlalchemy.orm.exc import NoResultFound

from db.contacts import Contact
from db.contact_addresses import ContactAddress
from config import Config
import os
import requests
from pbglobal.events.contactAdded import contactAdded
from pbglobal.events.contactRemoved import contactRemoved
from pbglobal.events.contactAddressAdded import contactAddressAdded
from pbglobal.pblib.pbrsa import encrypt, decrypt
from time import sleep
from datetime import datetime, timedelta
import urllib
import traceback
from pbglobal.commands.addStatisticalData import addStatisticalData
from pbglobal.pblib.amqp import client


def get_rooms_by_consumer_id(consumer_id):
    url = os.getenv("INTERNAL_ACCOUNTMANAGER_URL") + "/get_users"

    querystring = {"consumer_id": consumer_id}

    headers = {}

    response = requests.request("GET", url, headers=headers, params=querystring)
    data = response.json()
    rooms = []
    print("GOT THESE ROOMS", data)
    for room in data:
        rooms.append(room.get("username"))

    return rooms


def event_handler(ch, method=None, properties=None, body=None):
    try:
        if method.routing_key == "addContactAddress":
            payload = json.loads(body.decode())
            contactAddressAddedEvent = contactAddressAdded(skip_validation=True).from_json(contactAddressAdded, payload)
            contactAddressAddedEvent.publish()
            ch.basic_ack(method.delivery_tag)

            print("CONTACTADDRESS ADDED", contactAddressAdded)
            statisticalData = {
                "consumer": contactAddressAddedEvent.owner,
                "owner": contactAddressAddedEvent.owner,
                "creator": contactAddressAddedEvent.owner,
                "record_type": "contactAddressAdded",
                "record_data": {"checkin_time": contactAddressAddedEvent.addedtime,
                                "contact_address_id": payload.get("address_hash", "not set")}
            }
            try:
                addStatisticalData(skip_validation=True).from_json(addStatisticalData, statisticalData).publish()
            except:
                pass

        elif method.routing_key == "contactAddressAdded":
            payload = body.decode()
            payload_json = json.loads(payload)
            address_data = json.loads(payload_json.get("payload", {}))

            contact_address = None
            query = Config.db_session.query(ContactAddress).filter(
                ContactAddress.address_hash == payload_json.get("address_hash"))
            contact_address = query.first()
            Config.db_session.close()

            if not contact_address:
                contact_address = ContactAddress()

            contact_address.payload = payload

            contact_address.payload = json.dumps(address_data)

            contact_address.owner = urllib.parse.unquote(payload_json.get("owner", "not set"))
            contact_address.addedtime = payload_json.get("addedtime")
            contact_address.correlation_id = payload_json.get("correlation_id", "not set")
            contact_address.address_hash = payload_json.get("address_hash", "not set")

            contact_address.save()

            print("ContactAddress ID after insert:", contact_address.id)
            if type(contact_address.id) is not int:
                raise Exception("INSERT IN DATABASE HASN'T RETURNED AN ID")
                Config.db_session.commit()
                Config.db_session.close()

            try:
                ch.basic_ack(method.delivery_tag)
            except ChannelClosedByBroker:
                Config.db_session.commit()
                Config.db_session.close()

            exchange = "socketserver"
            routingKey = "commands"

            if payload_json.get("owner"):
                print("SENDING MESSAGE TO ROOM:", payload_json.get("owner"))
                message = {"command": "fireEvent",
                           "args": {"eventname": "contactAddressAdded", "eventdata": payload_json["payload"]}}
                payload = {"room": payload_json.get("owner"), "data": message}
                client().publish(toExchange=exchange, routingKey=routingKey, message=json.dumps(payload))

        elif method.routing_key == "addContact":
            payload = json.loads(body.decode())
            contactAddedEvent = contactAdded(skip_validation=True).from_json(contactAdded, payload)
            contactAddedEvent.publish()
            ch.basic_ack(method.delivery_tag)
            days = 30
            print("DELETEING ENTRIES OLDER THAN DAYS: ", days)
            sql = f'DELETE FROM contacts WHERE addedtime <= :to_date'
            to_date = datetime.today() - timedelta(days=days)
            Config.db_session.execute(sql, {"to_date": to_date})
            Config.db_session.commit()
            form_data = json.loads(decrypt(payload.get("form_data", {})))
            try:
                guest_amount = int(form_data.get("guest_amount", 1))
            except ValueError:
                guest_amount = 1

            statisticalData = {
                "owner": contactAddedEvent.consumer,
                "creator": contactAddedEvent.consumer,
                "consumer": contactAddedEvent.consumer,
                "login": contactAddedEvent.login,
                "record_type": "checkin",
                "record_data": {"checkin_time": contactAddedEvent.addedtime, "amount_of_persons": guest_amount,
                                "contact_id": payload.get("form_data_hash", "not set")}
            }

            addStatisticalData(skip_validation=True).from_json(addStatisticalData, statisticalData).publish()

        elif method.routing_key == "addContactV2":
            payload_json = json.loads(body.decode())
            print("HANDLING addContactV2 command 1", payload_json)
            query = Config.db_session.query(ContactAddress).filter(
                ContactAddress.address_hash == payload_json.get("hash"))
            contact_address = query.first()
            Config.db_session.close()
            if not contact_address:
                ch.basic_ack(method.delivery_tag)
                raise Exception("A contact address for the hash " + payload_json.get(
                    "hash") + "wasn't found although it should be there.")

            print("CONTACT ADDRESS payload", contact_address.payload)
            print("HANDLING addContactV2 command 2", json.loads(contact_address.payload if contact_address.payload is not None else "{}").get("payload", {}))
            addressData = json.loads(contact_address.payload if contact_address.payload is not None else "{}")
            print("CONTACT ADDRESSDATA", addressData)
            gateway_uri = os.getenv("APIGATEWAY_ADMIN_INTERNAL_BASEURL")
            url = gateway_uri + "/consumers/" + contact_address.owner

            headers = {}

            response = requests.request("GET", url, headers=headers)
            data = response.json()
            print("THATS THE REAL DEAL", data)


            contactAddedEvent = contactAdded(skip_validation=True)
            contactAddedEvent.consumer = data.get("username", contact_address.owner)
            contactAddedEvent.owner = contact_address.owner
            contactAddedEvent.creator = contact_address.owner
            contactAddedEvent.login = data.get("username", contact_address.owner)
            contactAddedEvent.correlation_id = contact_address.correlation_id
            contactAddedEvent.form_data = {
                "lastname": addressData.get("lastname", ""),
                "firstname": addressData.get("firstname", ""),
                "street": addressData.get("street", ""),
                "zipcode": addressData.get("zipcode", ""),
                "phone": addressData.get("phone", ""),
                "email": addressData.get("email", "")
            }
            contactAddedEvent.addedtime = payload_json.get("addedtime", None)
            contactAddedEvent.external_id = payload_json.get("external_id", "")
            contactAddedEvent.form_data_hash = contact_address.address_hash

            print("CONTACT ADDED EVENT", contactAddedEvent)
            contactAddedEvent.validate()
            contactAddedEvent.publish()

            ch.basic_ack(method.delivery_tag)

        elif method.routing_key == "contactAdded":
            contact = Contact()
            contact.payload = body.decode()
            payload_json = json.loads(contact.payload)
            print("CREATE CONTACT", payload_json)

            payload_json["email"] = encrypt(payload_json.get("email")) if payload_json.get("email") is not None and len(
                payload_json.get("email", "")) > 0 else payload_json.get("email")

            if type(payload_json.get("form_data")) != str:
                payload_json["form_data"] = encrypt(json.dumps(payload_json.get("form_data"))) if payload_json.get(
                    "form_data") is not None and len(payload_json.get("form_data", "")) > 0 else payload_json.get(
                    "form_data", "")

            contact.payload = json.dumps(payload_json)

            contact.consumer = urllib.parse.unquote(payload_json.get("consumer", "not set"))
            contact.login = urllib.parse.unquote(payload_json.get("login", "not set"))
            contact.external_id = payload_json.get("external_id", "not set")
            contact.addedtime = payload_json.get("addedtime")
            contact.correlation_id = payload_json.get("correlation_id", "not set")
            contact.form_data_hash = payload_json.get("form_data_hash", "not set")

            contact.save()
            print("Contact ID after insert:", contact.id)
            if type(contact.id) is not int:
                raise Exception("INSERT IN DATABASE HASN'T RETURNED AN ID")
            ch.basic_ack(method.delivery_tag)

        elif method.routing_key == "removeContact":
            contactRemovedEvent = contactRemoved(skip_validation=True).from_json(contactRemoved,
                                                                                 json.loads(body.decode()))
            contactRemovedEvent.publish()
            ch.basic_ack(method.delivery_tag)

        elif method.routing_key == "contactRemoved":
            payload = body.decode()
            payload_json = json.loads(payload)
            print("REMOVE CONTACT PAYLOAD", payload_json)
            contact = Config.db_session.query(Contact).filter_by(form_data_hash=payload_json.get("form_data_hash", "notavailable"),
                                                                 deletedtime=None).first()
            print("CONTACT FROM DATABASE", contact)
            if contact is None:
                contact = Config.db_session.query(Contact).filter_by(correlation_id=payload_json.get("external_id", ""),
                                                                     deletedtime=None).first()
                print("CONTACT FROM DATABASE BY EXTERNAL ID", contact)

            if contact is None:
                contact = Config.db_session.query(Contact).filter_by(external_id=payload_json.get("external_id", ""),
                                                                     deletedtime=None,
                                                                     consumer=payload_json.get("consumer")).first()
                print("CONTACT FROM DATABASE BY EXTERNAL ID", contact)

            if contact is None:
                contact = Contact()

            print("SET CONTACT LEAVE DATE", payload_json)

            if type(payload_json.get("form_data")) != str:
                payload_json["form_data"] = encrypt(json.dumps(payload_json.get("form_data"))) if payload_json.get(
                    "form_data") is not None and len(payload_json.get("form_data", "")) > 0 else payload_json.get(
                    "form_data", "")

            if contact.payload is not None:
                old_payload = json.loads(contact.payload)
                old_payload["deletedtime"] = payload_json.get("deletedtime")
                contact.payload = json.dumps(old_payload)
            else:
                contact.payload = json.dumps(payload_json)

            contact.consumer = urllib.parse.unquote(payload_json.get("consumer", "not set"))
            contact.login = urllib.parse.unquote(payload_json.get("login", "not set"))
            contact.external_id = payload_json.get("external_id", "not set")
            contact.deletedtime = payload_json.get("deletedtime")
            contact.correlation_id = payload_json.get("correlation_id", "not set")
            #contact.form_data_hash = payload_json.get("form_data_hash", "not set")

            contact.save()
            print("Contact ID after insert:", contact.id)
            if type(contact.id) is not int:
                raise Exception("INSERT IN DATABASE HASN'T RETURNED AN ID")
            ch.basic_ack(method.delivery_tag)


        else:
            print("NO HANDLER FOR routing key: ", method.routing_key)
            ch.basic_ack(method.delivery_tag)
    except Exception as e:
        print(e)
        traceback.print_exc()
        Config.db_session.rollback()
        if str(e) == "No row was found for one()":
            ch.basic_ack(method.delivery_tag)
        else:
            ch.basic_nack(method.delivery_tag)
        sleep(3)
