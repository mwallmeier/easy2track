#!/bin/sh
if ! test -f "/src/certs/id_rsa"; then
  echo "Creating rsa-key"
  ssh-keygen -m PEM -q -t rsa -N '' -f /src/certs/id_rsa 2>&1 >/dev/null
  ssh-keygen -e -f /src/certs/id_rsa.pub -m pem >/src/certs/id_rsa.pem
fi

python /src/app/server.py
