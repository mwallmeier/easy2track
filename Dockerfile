FROM planblick2/python3:latest

ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache \
            --upgrade \
            --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
       alpine-sdk \
       gcc musl-dev linux-headers libffi-dev openssl-dev jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev\
       tiff-dev tk-dev tcl-dev python3-dev build-base libxml2-dev libxslt-dev swig

RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing gnupg py3-m2crypto

RUN pip install --upgrade pip

COPY src/requirements.txt /src/app/requirements.txt
RUN pip3 install -r /src/app/requirements.txt

COPY src /src

EXPOSE 8000

RUN chmod 777 /src/runApp.sh
RUN chmod +x /src/runApp.sh

CMD ["/src/runApp.sh"]
